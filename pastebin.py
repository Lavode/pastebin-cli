# Copyright (c) 2012 Michael "Morrolan" Senn
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
# OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import pastebinapi
import argparse
import sys


def main():
    parser = argparse.ArgumentParser()

    # Verbose output or not.
    parser.add_argument("-v", "--verbose", help="Verbose output", action="store_true")

    # The API key which'll be used, required argument.
    parser.add_argument("-ak", "--apikey", help="API key", required=True)

    # Allows to specify username and password respectively a session key which'll be used to create a paste with a certain account.
    parser.add_argument("-u", "--user", help="Pastebin username")
    parser.add_argument("-p", "--password", help="Pastebin password")
    parser.add_argument("-uk", "--userkey", help="Session key")

    # Allows to create an unlisted/private paste.
    privacy_group = parser.add_mutually_exclusive_group()
    privacy_group.add_argument("--unlisted", help="Creates an unlisted paste", action="store_true")
    privacy_group.add_argument("--private", help="Creates a private paste", action="store_true")

    # Allows to choose an expiry date, "N" (Never), "10M" (10 Minutes), "1H" (1 Hour), "1D" (1 Day), "1M" (1 Month)
    parser.add_argument("--expiration", help="Expiration time for the paste", choices=list(pastebinapi.APIManager.ValidExpirySettings), default="N")

    # The paste's title
    parser.add_argument("--title", help="The paste's title", default="Untitled")

    # The paste's syntax highlighting
    parser.add_argument("--syntax", help="Desired syntax highlighting", choices=list(pastebinapi.APIManager.ValidFormattingSettings), default="text")

    # The input source to use, either it'll read from a file or it'll use input from the commandline. One of those has to be set
    input_group = parser.add_mutually_exclusive_group(required=True)
    input_group.add_argument("-f", "--file", help="File which to paste on Pastebin")
    input_group.add_argument("-t", "--text", help="Text which to paste on Pastebin")

    args = parser.parse_args()


    #print(args.verbose, args.user, args.password, args.userkey, args.apikey, args.file, args.text, sep="\n")

    manager = pastebinapi.APIManager()
    manager.APIKey = args.apikey

    # If both username and password are set, the session key will be overwritten. If only one or neither is set, the provided session key will be used.
    # If neither username nor password nor session key were supplied, the paste will be created as a guest.
    if (args.user is not None) and (args.password is not None):
        manager.UserKey = manager.do_login(args.user, args.password)
        use_authentication = True
    elif args.userkey is not None:
        manager.UserKey = args.userkey
        use_authentication = True
    else:
        # The paste will be created as a guest.
        use_authentication = False

    # If a file was specified it'll get read now, if a text was supplied it'll just get stored.
    if args.file is not None:
        try:
            file = open(args.file, "r")
            paste = file.read()
        except IOError:
            print("The specified file does not exist, or you lack permission to open it!")
            return 1
    elif args.text is not None:
        paste = args.text
    else:
        # Shouldn't really happen, since the argparser should take care of that.
        print("Either -f or -t has to be used!")
        return 2

    # Depending on the privacy flag which was set, the according integer will be set.
    if args.unlisted:
        privacy = 1
    elif args.private:
        privacy = 2
    else:
        privacy = 0

    try:
        url = manager.create_paste(paste, args.title, args.syntax, privacy, args.expiration, use_authentication)
        print("Successfully created a paste at", url)
    except pastebinapi.APIError as e:
        print("An error occured while creating the paste!", e.Query, e.Result, e.Message, sep="\n")




if __name__ == "__main__":
    sys.exit(main())


